<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<script type="text/javascript">

var values = {};

$(document).ready(function() {
	table = $('#example').DataTable({
    	"ordering": false,
    	"searching": false,
    	"info": false,
    	//paging: false,
    	data: [],
        columns: [{ title: "ID" },{ title: "Name" }]
  	});
	
	$('#example tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        findById(data[0]);
    } );
} );

function allCharacters(){	
	$.get("home/getAllComics", function(response) {
		  $(".result").html(response);
		  
		  $('#example').dataTable().fnClearTable();
		  
		  var values = [];
		  
		  response.data[0].results.forEach(function(item) {
			  values.push([item.id,item.name]);
          });		  

		  table.rows.add(values).draw();
	});
}

function findById(id){
	$.get("home/getById?id="+id,function(response) {
		  $(".result").html(response);
		  
		  $('#example').dataTable().fnClearTable();
		  
		  var values = [];
		  
		  response.data[0].results.forEach(function(item) {
			  values.push([item.id,item.name]);
          });		  

		  table.rows.add(values).draw();
	});
	
	
}
	
</script>

</head>
<body>

	<center>
		
	<h3>
		<a href="#" onclick="allCharacters();">Search All Characters</a>
	</h3>
	
    <table id="example" class="display" width="100%"></table>

	</center>
	
</body>
</html>