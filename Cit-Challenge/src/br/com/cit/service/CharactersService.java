package br.com.cit.service;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import br.com.cit.pojo.MarvelResponsePojo;

public class CharactersService {

	public static final String PUBLIC_KEY = "617be859291c0882aa722c6b6ce3d9aa";
	public static final String PRIVATE_KEY = "3cac20b864f59dfff139283325db7b4b5389c371";
	public static final String URL = "https://gateway.marvel.com/v1/public/characters";

	public MarvelResponsePojo getCharacters(Integer... id) {
		try {

			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.configure(DeserializationConfig.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

			return mapper.readValue(getMarvelJson(id), MarvelResponsePojo.class);

		} catch (NoSuchAlgorithmException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getMarvelJson(Integer... id) throws NoSuchAlgorithmException {

		Long ts = new Date().getTime();
		String hash = convertToMD5(ts + PRIVATE_KEY + PUBLIC_KEY);
		String key = "ts=" + ts.toString() + "&apikey=" + PUBLIC_KEY + "&hash=" + hash;

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = null;

		if (id.length == 0) {
			webTarget = client.target(URL + "?limit=100&" + key);
		} else {
			webTarget = client.target(URL + "/" + id[0] + "?" + key);
		}

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

		Response response = invocationBuilder.get();
		String json = response.readEntity(String.class);
		response.close();

		return json;
	}

	private String convertToMD5(String value) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		BigInteger hash = new BigInteger(1, md.digest(value.getBytes()));
		return String.format("%32x", hash);
	}
}
