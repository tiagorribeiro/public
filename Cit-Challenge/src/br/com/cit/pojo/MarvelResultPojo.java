package br.com.cit.pojo;

import java.util.List;

public class MarvelResultPojo {

	private Integer total;

	private List<MarvelCharactersPojo> results;

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public List<MarvelCharactersPojo> getResults() {
		return results;
	}

	public void setResults(List<MarvelCharactersPojo> results) {
		this.results = results;
	}

}
