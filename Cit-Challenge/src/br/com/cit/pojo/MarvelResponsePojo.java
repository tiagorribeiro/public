package br.com.cit.pojo;

import java.util.List;

public class MarvelResponsePojo {

	private List<MarvelResultPojo> data;

	public List<MarvelResultPojo> getData() {
		return data;
	}

	public void setData(List<MarvelResultPojo> data) {
		this.data = data;
	}

}
