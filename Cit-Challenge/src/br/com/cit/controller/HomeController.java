package br.com.cit.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import br.com.cit.pojo.MarvelResponsePojo;
import br.com.cit.service.CharactersService;

@RestController
@RequestMapping("/home")
@EnableWebMvc
public class HomeController {

	@RequestMapping(value = "/getAllComics", produces = MediaType.APPLICATION_JSON, method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<MarvelResponsePojo> boasVindas() {
		CharactersService charactersService = new CharactersService();
		return new ResponseEntity<>(charactersService.getCharacters(), HttpStatus.OK);
	}

	@RequestMapping(value = "/getById{id}", produces = MediaType.APPLICATION_JSON, method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<MarvelResponsePojo> getById(@RequestParam(value = "id") Integer id) {
		CharactersService charactersService = new CharactersService();
		return new ResponseEntity<>(charactersService.getCharacters(id), HttpStatus.OK);
	}
}
